import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';

import ClientContainer from './client/client-container'
import DeviceContainer from './device/device-container'
import SensorContainer from './sensor/sensor-container'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';




class App extends React.Component {


    render() {

        return (
            <div className={styles.back}>
                <Router>
                    <div>
                        <NavigationBar />
                        <Switch>

                            <Route
                                exact
                                path='/'
                                render={() => <Home/>}
                            />


                            <Route
                                exact
                                path='/client'
                                render={() => <ClientContainer/>}
                            />

                            <Route
                                exact
                                path='/device'
                                render={() => <DeviceContainer/>}
                            />


                            <Route
                                exact
                                path='/sensor'
                                render={() => <SensorContainer/>}
                            />
                            {/*Error*/}
                            <Route
                                exact
                                path='/error'
                                render={() => <ErrorPage/>}
                            />

                            <Route render={() =><ErrorPage/>} />
                        </Switch>
                    </div>
                </Router>
            </div>
        )
    };
}

export default App
