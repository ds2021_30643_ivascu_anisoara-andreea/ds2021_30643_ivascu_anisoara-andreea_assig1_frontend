import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    login: '/home-login'
};

function authentication(user, callback) {
    const request = new Request(HOST.backend_api + endpoint.login, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback)

}
export {
    authentication
};