import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';

import * as API_USERS from "./api/client-api"

import ClientForm from "./components/client-form";
import ClientTable from "./components/client-table";


import ClientFormUpdate from "./components/client-form-update";




const updateStyle ={
    marginLeft:"20px"
}

class ClientContainer extends React.Component {

    constructor(props) {
        super(props);

        this.toggleFormClient = this.toggleFormClient.bind(this);


        this.updateToggleFormClient = this.updateToggleFormClient.bind(this);
        this.reloadClient = this.reloadClient.bind(this);
        this.updateReloadClient = this.updateReloadClient.bind(this);
        this.reloadAfterDelete = this.reloadAfterDelete.bind(this);
        this.deleteClient = this.deleteClient.bind(this);
        this.updateClient = this.updateClient.bind(this);

        this.state = {


            updatedClient:false,
            clientData: null,
            selectedClient: false,
            collapseFormClient: false,
            tableDataClient: [],
            isLoadedClient: false,
            errorStatusClient: 0,
            errorClient: null,


        };
    }

    componentDidMount() {
        this.fetchClients();

    }



    deleteClient(client){
        API_USERS.deleteClient(client,(result, status, err) => this.reloadAfterDelete());
    }



    updateClient(client) {
        this.state.clientData = client;
        this.updateToggleFormClient();
    }



    fetchClients() {
        return API_USERS.getClients((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableDataClient: result,
                    isLoadedClient: true
                });
            } else {
                this.setState(({
                    errorStatusClient: status,
                    errorClient: err
                }));
            }
        });
    }




    toggleFormClient() {
        this.setState({selectedClient: !this.state.selectedClient});
    }

    updateToggleFormClient(){
        this.setState({updatedClient: !this.state.updatedClient});
    }


    reloadClient(){
        this.setState({
            isLoadedClient:false
        });
        this.toggleFormClient();

        this.fetchClients();

    }


    updateReloadClient() {
        this.setState({
            isLoadedClient: false
        });

        this.updateToggleFormClient();
        this.fetchClients();

    }

    reloadAfterDelete() {
        this.setState({
            isLoadedClient: false
        });
        this.fetchClients();
    }



    render() {

        return (
            <div>

                <CardHeader>
                    <strong> Client Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleFormClient}>Add Client </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoadedClient && <ClientTable tableData = {this.state.tableDataClient} update={this.updateClient} delete={this.deleteClient}/>}
                            {this.state.errorStatusClient > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatusClient}
                                error={this.state.errorClient}
                            />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selectedClient} toggle={this.toggleFormClient}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormClient}> Add Client: </ModalHeader>
                    <ModalBody>
                        <ClientForm reloadHandler={this.reloadClient}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.updatedClient} toggle={this.updateToggleFormClient}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.updateToggleFormClient}> Update </ModalHeader>
                    <ModalBody>
                        <ClientFormUpdate reloadHandler={this.updateReloadClient} selectedClient = {this.state.clientData}/>
                    </ModalBody>
                </Modal>


            </div>


        )

    }
}


export default ClientContainer;
